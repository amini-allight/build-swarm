function saveFile(name)
{
    var link = document.createElement("a");
    link.download = name;
    link.href = "/control-panel/file/" + name;
    link.click();
}

function requestBuild()
{
    fetch("/control-panel/build");
}

function requestCancel()
{
    fetch("/control-panel/cancel");
}

function onError()
{
    var workers = document.getElementById("workers");

    workers.innerHTML = '<span class="workers-message">Connection to the server has been disrupted.</span>';
}

function onDownload(download)
{
    for (var i = 0; i < download.length; i++)
    {
        saveFile(download[i].log_name);

        if (download[i].success)
        {
            saveFile(download[i].file_name);
        }
    }
}

function requestDownload()
{
    fetch("/control-panel/download")
        .then(res => res.json())
        .then(json => onDownload(json))
        .catch(ex => onError(ex));
}

function onStatus(status)
{
    var workers = document.getElementById("workers");

    var html = "";

    if (status.workers.length != 0)
    {
        for (var i = 0; i < status.workers.length; i++)
        {
            var worker = status.workers[i];

            html += '<li class="worker">';
            html += '<span class="worker-id">' + worker.id + '</span>';
            html += '<div class="worker-active ' + (worker.active ? "active" : "inactive") + '"></div>';
            html += '<span class="worker-build-state">' + worker.build_state + '</span>';
            html += '</li>';
        }
    }
    else
    {
        html += '<span class="workers-message">No workers have been detected.</span>';
    }

    workers.innerHTML = html;
}

function getStatus()
{
    fetch("/control-panel/status")
        .then(res => res.json())
        .then(json => onStatus(json))
        .catch(ex => onError(ex));
}

getStatus();
setInterval(getStatus, 1000);
