#!/bin/crystal run
# Build Swarm, a system for super simple cross-platform build automation.
# Copyright (C) 2023 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "http/server"
require "json"
require "ecr"
require "./constants"
require "./http_tools"
require "./controller"
require "./messages"

`git clone -b #{BUILD_SCRIPTS_BRANCH} "#{BUILD_SCRIPTS_URL}" "#{BUILD_SCRIPTS_PATH}"`

CONTROLLER = Controller.new

def extract_suffix(context : HTTP::Server::Context, head : String) : String?
    tail = context.request.path[head.size, context.request.path.size - head.size]

    if tail =~ /\/.+/
        tail.lchop
    else
        nil
    end
end

def handle_home(context : HTTP::Server::Context) : Void
    ok_response context, "text/html", ECR.render("res/index.html.ecr")
end

def handle_control_panel_status(context : HTTP::Server::Context) : Void
    ok_response context, "application/json", CONTROLLER.status.to_json
end

def handle_control_panel_build(context : HTTP::Server::Context) : Void
    `cd "#{BUILD_SCRIPTS_PATH}" && git pull origin #{BUILD_SCRIPTS_BRANCH}`

    CONTROLLER.begin_build

    ok_response context
end

def handle_control_panel_cancel(context : HTTP::Server::Context) : Void
    CONTROLLER.cancel_build

    ok_response context
end

def handle_control_panel_download(context : HTTP::Server::Context) : Void
    ok_response context, "application/json", CONTROLLER.download.to_json
end

def handle_control_panel_file(context : HTTP::Server::Context) : Void
    file_name = extract_suffix(context, "/control-panel/file")

    if file_name.nil?
        bad_request_response context
        return
    end

    file_data = CONTROLLER.get file_name

    if file_data.nil?
        not_found_response context
        return
    end

    ok_response context, "application/octet-stream", String.new(Base64.decode(file_data))
end

def handle_worker_poll(context : HTTP::Server::Context) : Void
    begin
        message = WorkerPollMessage.from_json context.request.body.as(IO)

        state = CONTROLLER.poll message.worker_id

        ok_response context, "application/json", state.to_json
    rescue e : Exception
        internal_server_error_response context
    end
end

def handle_worker_upload(context : HTTP::Server::Context) : Void
    begin
        message = WorkerUploadMessage.from_json context.request.body.as(IO)

        CONTROLLER.upload message.worker_id, message.result

        ok_response context
    rescue e : Exception
        internal_server_error_response context
    end
end

def handle_favicon(context : HTTP::Server::Context) : Void
    ok_response context, "image/x-icon", File.read("#{RESOURCE_PATH}/favicon.ico")
end

def handle_static(context : HTTP::Server::Context) : Void
    if context.request.path.includes? ".."
        not_found_response context
        return
    end

    file_path = context.request.path[STATIC_PREFIX.size, context.request.path.size - STATIC_PREFIX.size]

    full_path = "#{STATIC_PATH}/#{file_path}"

    if !File.exists? full_path
        not_found_response context
        return
    end

    content_type = ""

    case context.request.path
    when .ends_with? ".html"
        content_type = "text/html"
    when .ends_with? ".css"
        content_type = "text/css"
    when .ends_with? ".js"
        content_type = "application/ecmascript"
    when .ends_with? ".json"
        content_type = "application/json"
    when .ends_with? ".png"
        content_type = "image/png"
    when .ends_with? ".ttf"
        content_type = "font/ttf"
    when .ends_with? ".txt"
    else
        content_type = "text/plain"
    end

    file = File.read full_path

    ok_response context, content_type, file
end

def handle_arch(context : HTTP::Server::Context) : Void
    suffix = extract_suffix context, "/setup/arch"

    ok_response context, "text/x-shellscript", ECR.render("res/setup/arch.sh.ecr")
end

def handle_debian(context : HTTP::Server::Context) : Void
    suffix = extract_suffix context, "/setup/debian"

    ok_response context, "text/x-shellscript", ECR.render("res/setup/debian.sh.ecr")
end

def handle_rhel(context : HTTP::Server::Context) : Void
    suffix = extract_suffix context, "/setup/rhel"

    ok_response context, "text/x-shellscript", ECR.render("res/setup/rhel.sh.ecr")
end

def handle_windows(context : HTTP::Server::Context) : Void
    suffix = extract_suffix context, "/setup/windows"

    ok_response context, "text/x-shellscript", ECR.render("res/setup/windows.sh.ecr")
end

def handle_macos(context : HTTP::Server::Context) : Void
    suffix = extract_suffix context, "/setup/macos"

    ok_response context, "text/x-shellscript", ECR.render("res/setup/macos.sh.ecr")
end

def handle_freebsd(context : HTTP::Server::Context) : Void
    suffix = extract_suffix context, "/setup/freebsd"

    ok_response context, "text/x-shellscript", ECR.render("res/setup/freebsd.sh.ecr")
end

server = HTTP::Server.new do |context|
    case context.request.path
    when /^\/$/ then handle_home context
    when /^\/control-panel\/status$/ then handle_control_panel_status context
    when /^\/control-panel\/build$/ then handle_control_panel_build context
    when /^\/control-panel\/cancel$/ then handle_control_panel_cancel context
    when /^\/control-panel\/download$/ then handle_control_panel_download context
    when /^\/control-panel\/file\/.*$/ then handle_control_panel_file context
    when /^\/worker\/poll$/ then handle_worker_poll context
    when /^\/worker\/upload$/ then handle_worker_upload context
    when /^\/favicon\.ico$/ then handle_favicon context
    when /^\/static\/.*$/ then handle_static context
    when /^\/setup\/arch(\/.*)?$/ then handle_arch context
    when /^\/setup\/debian(\/.*)?$/ then handle_debian context
    when /^\/setup\/rhel(\/.*)?$/ then handle_rhel context
    when /^\/setup\/windows(\/.*)?$/ then handle_windows context
    when /^\/setup\/macos(\/.*)?$/ then handle_macos context
    when /^\/setup\/freebsd(\/.*)?$/ then handle_freebsd context
    else not_found_response context
    end
end

puts "Server online at #{HOST}:#{PORT}"
server.listen HOST, PORT
