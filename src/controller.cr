# Build Swarm, a system for super simple cross-platform build automation.
# Copyright (C) 2023 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./worker"
require "./status"

class Controller
    def initialize
        @workers = {} of String => Worker
    end

    def status : Status
        Status.new @workers.values.map &.status
    end

    def begin_build : Void
        @workers.values.select{ |x| x.active? }.each &.begin_build
    end

    def cancel_build : Void
        @workers.values.select{ |x| x.active? }.each &.cancel_build
    end

    def build_complete? : Bool
        @workers.values.all? &.build_complete?
    end

    def poll(worker_id : String) : WorkerState
        @workers[worker_id] = Worker.new worker_id if !@workers.has_key? worker_id

        @workers[worker_id].poll
    end

    def upload(worker_id : String, result : BuildResult) : Void
        return if !@workers.has_key? worker_id

        @workers[worker_id].upload result
    end

    def download : Array(DownloadInfo)
        @workers.values.map(&.download).reject(&.nil?).map(&.as(DownloadInfo))
    end

    def get(file_name : String) : String?
        @workers.each_value do |worker|
            data = worker.get file_name

            return data if !data.nil?
        end

        nil
    end
end
