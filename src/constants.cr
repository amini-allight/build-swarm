# Build Swarm, a system for super simple cross-platform build automation.
# Copyright (C) 2023 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./tools"

if !ENV.has_key? "BUILD_SWARM_HOST"
    puts "The 'BUILD_SWARM_HOST' environment variable is required."
    exit 1
end

if !ENV.has_key? "BUILD_SWARM_PORT"
    puts "The 'BUILD_SWARM_PORT' environment variable is required."
    exit 1
end

if ARGV.size != 2
    puts "Usage: ./src/main.cr -- https://my-username:my-password@github.com/my-username/my-build-scripts branch"
    exit 1
end

RESOURCE_PATH = "res"
STATIC_PATH = "#{RESOURCE_PATH}/static"
WORKER_TIMEOUT = 10 * 1000
TEMP_PATH = "/tmp"
STATIC_PREFIX = "/static/"
BUILD_SCRIPTS_PATH = "#{TEMP_PATH}/build-swarm-#{random_file_name}"
HOST = ENV["BUILD_SWARM_HOST"]
PORT = ENV["BUILD_SWARM_PORT"].to_u16
SHARED_ENV = ENV.select{ |k, v| k.starts_with? "BUILD_SWARM_" }.to_h
BUILD_SCRIPTS_URL = ARGV[0]
BUILD_SCRIPTS_BRANCH = ARGV[1]
