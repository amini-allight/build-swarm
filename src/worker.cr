# Build Swarm, a system for super simple cross-platform build automation.
# Copyright (C) 2023 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./constants"
require "./tools"
require "./worker_state"
require "./worker_status"
require "./build_result"
require "./download_info"

enum BuildState
    Idle
    ShouldBuild
    ShouldCancel
    Building
    Succeeded
    Failed
end

class Worker
    getter id
    @last_seen : Int64

    def initialize(@id : String)
        @build_state = BuildState::Idle
        @build_result = BuildResult.new
        @last_seen = current_time
    end

    def build_complete? : Bool
        @build_state == BuildState::Succeeded || @build_state == BuildState::Failed
    end

    def mark_active : Void
        @last_seen = current_time
    end

    def active? : Bool
        current_time - @last_seen < WORKER_TIMEOUT
    end

    def begin_build : Void
        @build_state = BuildState::ShouldBuild
        @build_result = BuildResult.new
    end

    def cancel_build : Void
        @build_state = BuildState::ShouldCancel
        @build_result = BuildResult.new
    end

    def poll : WorkerState
        mark_active

        state = WorkerState.new @build_state.to_s.underscore, build_script

        case @build_state
        when BuildState::ShouldBuild
            @build_state = BuildState::Building
        when BuildState::ShouldCancel
            @build_state = BuildState::Idle
        end

        state
    end
    
    def upload(result : BuildResult) : Void
        if result.file.empty?
            @build_state = BuildState::Failed
        else
            @build_state = BuildState::Succeeded
        end

        @build_result = result
    end

    def download : DownloadInfo?
        case @build_state
        when BuildState::Succeeded, BuildState::Failed
            DownloadInfo.new "#{id}.txt", @build_result.file_name, @build_state == BuildState::Succeeded
        else
            nil
        end
    end

    def get(file_name : String) : String?
        case @build_state
        when BuildState::Succeeded, BuildState::Failed
            if file_name == "#{id}.txt"
                @build_result.log
            elsif file_name == @build_result.file_name
                @build_result.file
            else
                nil
            end
        else
            nil
        end
    end

    def status : WorkerStatus
        WorkerStatus.new id, active?, @build_state.to_s.underscore
    end

    def build_script : String
        path = "#{BUILD_SCRIPTS_PATH}/#{id}.sh"

        return "" if !File.exists? path

        File.read path
    end
end
