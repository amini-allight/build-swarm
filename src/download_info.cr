# Build Swarm, a system for super simple cross-platform build automation.
# Copyright (C) 2023 Amini Allight
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "json"

struct DownloadInfo
    property log_name
    property file_name
    property success

    def initialize
        @log_name = ""
        @file_name = ""
        @success = false
    end

    def initialize(@log_name : String, @file_name : String, @success : Bool)
    end

    include JSON::Serializable
end
